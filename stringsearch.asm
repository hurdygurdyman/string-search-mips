.text
main:
la $a0, substring
jal strlen # Calculate the size of the subsubstring to be searched

# Load the addresses of the strings
la $a0, string
la $a1, substring

# Start comparing
move $a2, $v0
move $a3, $zero # Set the position counter to 0

subssearch_loop:
jal strncmp # Call comparator
beqz $v0, subssearch_return # If the subsubstring was found
addi $a0, $a0, 1 # Argument registers are not overwritten in the strncmp call
addi $a3, $a3, 1
j subssearch_loop

subssearch_return:
move $v0, $a3 # Return the current position
j end_subssearch

# Substring comparator
strncmp: 

# Be aware: the program will return the lenght of the string if the substring isn`t found
move $t0, $a0 # String address
move $t1, $a1 # Substring address
move $t2, $a2 # The size of the substring

strncmp_loop:
beqz $t2, strncmp_equal # End of comparison
lb $t3, 0($t0) # Load a byte from string
lb $t4, 0($t1) # Load a byte from substring
beqz $t3, strncmp_checklower # String is over
beqz $t4, strncmp_higher # Substring is over
blt $t3, $t4, strncmp_lower # String has a lower value
bgt $t3, $t4, strncmp_higher # String has a higher value
addi $t0, $t0, 1 # Iterate
addi $t1, $t1, 1
subi $t2, $t2, 1
j strncmp_loop

strncmp_checklower:
beqz $t4, strncmp_equal
j strncmp_lower

strncmp_equal:
li $v0, 0
jr $ra

strncmp_lower:
li $v0, -1
jr $ra

strncmp_higher:
li $v0, 1
jr $ra

strlen:
li $t0, 0 # Initialize the counter
strlen_loop:
lb $t1, 0($a0) # Load the next character into t1
beqz $t1, strlen_return # Check for the null character
addi $a0, $a0, 1 # Go to the next character of the string
addi $t0, $t0, 1 # Increment the counter
j strlen_loop

strlen_return:
move $v0, $t0
jr $ra

end_subssearch:
move $a0, $v0

# Print the result
addi $v0, $zero, 1
syscall

# Leave the program
move $a0, $zero
addi $v0, $zero, 10
syscall

.data
string: .asciiz "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
substring: .asciiz "lorem ipsum"
