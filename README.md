# string-search-mips
A naïve string search implementation in MIPS assembly.
Developed and tested with the MARS simulator (http://courses.missouristate.edu/KenVollmar/MARS/)
This code will simply search a substring and output it's position. It implements strlen and strcmpi functions.
